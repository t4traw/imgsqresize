require "rubygems"
require "RMagick"

class ImgSqResizer

	def sqResize(imagefile,imagesize)
		@file = Magick::Image.read("#{imagefile}.jpg").first
		@size = imagesize

		@file.resize_to_fit!(@size,@size) #普通のresizeだけだと比率保ってくれないので注意
		back_img = Magick::Image.new(@size, @size){
			self.background_color = '#ffffff'
		}
		back_img.composite(@file, Magick::CenterGravity, 
			Magick::OverCompositeOp).write("kansei_#{@size}.jpg")
  end

end

hoge = ImgSqResizer.new()
hoge.sqResize("sample",160)
hoge.sqResize("sample",400)
